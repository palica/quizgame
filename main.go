package main

import (
	"encoding/csv"
	"flag"
	"fmt"
	"log"
	"os"
	"os/exec"
	"time"
)

func main() {
	csvFilename := flag.String("csv", "problems.csv", "you can specify your file with quiz questions, default problems.csv")
	flag.Parse()

	file, err := os.Open(*csvFilename)
	if err != nil {
		log.Fatal(err)
	}

	r := csv.NewReader(file)
	lines, err := r.ReadAll()
	if err != nil {
		log.Fatal(err)
	}
	fmt.Println(lines)

	correct := 0
	for problems := range lines {
		cmd := exec.Command("clear")
		cmd.Stdout = os.Stdout
		cmd.Run()

		fmt.Printf("Odpovedali ste spravne na %d/%d otazok.\n", correct, len(lines))
		fmt.Println("Problém číslo", problems+1)
		fmt.Println("Otázka:", lines[problems][0])
		var answer string
		fmt.Println("Napíšte vašu odpoveď:")
		fmt.Scanln(&answer)
		if answer == lines[problems][1] {
			fmt.Println("SPRAVNE!")
			correct++
			time.Sleep(2 * time.Second)
			fmt.Printf("Odpovedali ste spravne na %d/%d otazok.\n", correct, len(lines))
		} else {
			fmt.Println("NESPRAVNE!")
			fmt.Println("Spravna odpoveď:", lines[problems][1])
			time.Sleep(2 * time.Second)
			fmt.Printf("Odpovedali ste spravne na %d/%d otazok.\n", correct, len(lines))
		}

	}

}
